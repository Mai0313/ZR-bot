
import hashlib
from unittest.mock import patch

import autorootcwd  # noqa: F401

from captcha_resolver import CaptchaResolver, HWIDVerify


def test_get_windows_hwid():
    with patch('subprocess.getoutput') as mock_getoutput:
        mock_getoutput.return_value = "Command Output\n\nUUID_VALUE\nExtra Line"
        hwid = HWIDVerify().get_windows_hwid()
        assert hwid == "UUID_VALUE"

def test_get_linux_hwid():
    with patch('subprocess.getoutput') as mock_getoutput:
        mock_getoutput.side_effect = ["cpu_serial", "motherboard_serial", "mac_address"]
        hw_info = "cpu_serial" + "motherboard_serial" + "mac_address"  # noqa: ISC003
        expected_hwid = hashlib.sha256(hw_info.encode()).hexdigest()
        hwid = HWIDVerify().get_linux_hwid()
        assert hwid == expected_hwid

import base64
import io
from unittest.mock import patch

from captcha_resolver import CaptchaResolver


def test_get_captcha_code_success():
    with patch('requests.post') as mock_post:
        mock_response = mock_post.return_value
        mock_response.status_code = 200
        mock_response.json.return_value = {'result': 'captcha_code'}
        result = CaptchaResolver().get_captcha_code("image_input")
        assert result == "captcha_code"

def test_get_captcha_code_error():
    with patch('requests.post') as mock_post, patch('builtins.print') as mock_print:
        mock_response = mock_post.return_value
        mock_response.status_code = 403
        mock_response.json.return_value = {'result': 'error', 'message': 'error_message'}
        CaptchaResolver().get_captcha_code("image_input")
        mock_print.assert_called_with("error: error_message")

def test_image_to_base64():
    image_path = "test/data/2BEAB.png"
    with patch('builtins.open', return_value=io.BytesIO(b'image_data')) as mock_open:
        result = CaptchaResolver().image_to_base64(image_path)
        assert result == base64.b64encode(b'image_data').decode('utf-8')


from unittest.mock import AsyncMock

import pytest

from src.get_payment_code import PaymentDetailsFetcher


@pytest.mark.asyncio
async def test_get_payment_details():
    # 建立模擬物件
    mock_playwright = AsyncMock()
    mock_browser = AsyncMock()
    mock_context = AsyncMock()
    mock_page = AsyncMock()

    # 設置模擬的返回值
    mock_playwright.__aenter__.return_value = mock_playwright
    mock_playwright.chromium.launch.return_value = mock_browser
    mock_browser.new_context.return_value = mock_context
    mock_context.new_page.return_value = mock_page
    mock_page.text_content.side_effect = ["payment_code", "payment_amount", "payment_date", "order_id_temp", "order_id"]
    mock_page.screenshot.return_value = b'screenshot_data'

    # 初始化PaymentDetailsFetcher並設置模擬物件
    fetcher = PaymentDetailsFetcher()
    fetcher.playwright = mock_playwright
    fetcher.browser = mock_browser

    # 調用方法並檢查結果
    payment_code, payment_amount, payment_date, order_id, screenshot = await fetcher.get_payment_details(100)

    assert payment_code == "payment_code"
    assert payment_amount == "payment_amount"
    assert payment_date == "payment_date"
    assert order_id == "order_id"
    assert screenshot == b'screenshot_data'


from unittest.mock import MagicMock

import pytest

from src.get_payment_line import PaymentDetailsFetcherSync


def test_get_payment_details_sync():
    # 建立模擬物件
    mock_playwright = MagicMock()
    mock_browser = MagicMock()
    mock_context = MagicMock()
    mock_page = MagicMock()
    mock_element = MagicMock()

    # 設置模擬的返回值
    mock_playwright.start.return_value = mock_playwright
    mock_playwright.chromium.launch.return_value = mock_browser
    mock_browser.new_context.return_value = mock_context
    mock_context.new_page.return_value = mock_page
    mock_page.text_content.side_effect = ["payment_code", "payment_amount", "payment_date", "order_id_temp", "order_id"]
    mock_page.screenshot.return_value = b'screenshot_data'
    mock_element.bounding_box.return_value = {"x": 0, "y": 0, "width": 100, "height": 100}
    mock_page.query_selector.return_value = mock_element

    # 初始化PaymentDetailsFetcherSync並設置模擬物件
    fetcher = PaymentDetailsFetcherSync()
    fetcher.playwright = mock_playwright
    fetcher.browser = mock_browser

    # 調用方法並檢查結果
    payment_code, payment_amount, payment_date, order_id, screenshot = fetcher.get_payment_details(100)

    assert payment_code == "payment_code"
    assert payment_amount == "payment_amount"
    assert payment_date == "payment_date"
    assert order_id == "order_id"
    assert screenshot == b'screenshot_data'
