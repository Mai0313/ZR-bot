import os

from dotenv import load_dotenv
from omegaconf import OmegaConf

cfg = OmegaConf.load("configs/setting.yaml")

load_dotenv()

LINE_CHANNEL_ACCESS_TOKEN = os.getenv('LINE_CHANNEL_ACCESS_TOKEN')
LINE_CHANNEL_SECRET = os.getenv('LINE_CHANNEL_SECRET')

NGROK_TOKEN = os.getenv('NGROK_TOKEN')
DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')
DISCORD_CHANNEL_ID = os.getenv('DISCORD_CHANNEL_ID')
line_dev_account = os.getenv('line_dev_account')
line_dev_password = os.getenv('line_dev_password')

OUTPUT_DIR = cfg.database_dir.outputfile
ACCOUNT_FILE = cfg.database_dir.account_filepath
PAYMENT_FILE = cfg.database_dir.payment_filepath

PAYMENT_UPDATE_LINK = cfg.payment.search_payment_link
PAYMENT_LINK = cfg.payment.payment_link
PAYMENT_FEE = cfg.payment.payment_fee
PAYMENT_EMAIL = cfg.payment.payment_email

LINE_NOTIFY_TOKEN = cfg.line_notify.token
