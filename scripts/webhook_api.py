from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import autorootcwd  # noqa: F401
from environment import line_dev_account, line_dev_password

class WebhookUpdater:
    """這個類主要的目的是模擬使用者來更新 webhook URL。"""
    def __init__(self):
        self.driver = None

    def start_browser(self):
        self.driver = webdriver.Chrome()
        self.driver.set_window_size(1548, 1032)

    def update_webhook(self, external_url, line_channel_url):
        print(line_channel_url)
        self.driver.get(line_channel_url)
        self.driver.find_element(By.CSS_SELECTOR, ".btn-primary").click()
        self.driver.find_element(By.NAME, "tid").send_keys(line_dev_account)
        self.driver.find_element(By.NAME, "tpasswd").send_keys(line_dev_password)
        self.driver.find_element(By.CSS_SELECTOR, ".MdBtn01").click()
        WebDriverWait(self.driver, 60).until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".outline")))
        self.driver.find_element(By.CSS_SELECTOR, ".outline").click()
        self.driver.find_element(By.CSS_SELECTOR, ".tab-section:nth-child(3) > .kv-field").click()
        user_input_element = self.driver.find_element(By.CSS_SELECTOR, ".user-input")
        user_input_element.clear()
        user_input_element.send_keys(f"{external_url}/callback")
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".kv-button:nth-child(1)"))).click()

    def run(self, external_url, line_channel_url):
        self.start_browser()
        self.update_webhook(external_url, line_channel_url)

if __name__ == '__main__':
    import sys
    external_url = sys.argv[1]
    line_channel_url = sys.argv[2]

    updater = WebhookUpdater()
    updater.run(external_url, line_channel_url)
