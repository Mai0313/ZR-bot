#!/bin/bash

# 檢查conda命令是否存在
curl -sSL https://github.com/Mai0313/python-setup/raw/master/linux/setup_conda.sh | bash

# 手動設置PATH
export PATH="$HOME/miniconda3/bin:$PATH"

# 初始化conda
eval "$(~/miniconda3/bin/conda shell.bash hook)"

conda activate base
curl -sSL https://github.com/Mai0313/python-setup/raw/master/linux/setup_dep.sh | bash

playwright install chromium



# curl -sSL https://github.com/Mai0313/python-setup/raw/master/linux/uninstall.sh | bash
