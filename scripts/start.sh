#!/bin/bash

# 檢查 Python 是否已安裝
if ! command -v python &> /dev/null; then
    echo "Python 沒有安裝。請從以下連結安裝: https://www.python.org/ftp/python/3.9.13/python-3.9.13-amd64.exe"
    exit 1
fi

# 安裝 requirements.txt 中的套件
echo "正在安裝 requirements.txt 中的套件..."
pip install -r requirements.txt || { echo "安裝 requirements.txt 失敗，請檢查。"; exit 1; }

# 安裝 Playwright 的 Chromium
echo "正在安裝 Playwright 的 Chromium..."
playwright install chromium || { echo "安裝 Chromium 失敗，請檢查。"; exit 1; }

# 檢查 .env 文件是否存在
if [ ! -f .env ]; then
    echo ".env 文件不存在，正在創建..."
    read -p "請輸入 Line Channel ID (純數字) 請至Line Developer -> Basic Settings 獲取: " LINE_CHANNEL_ID
    echo "LINE_CHANNEL_ID=$LINE_CHANNEL_ID" >> .env

    read -p "請輸入 Channel access token 請至 https://developers.line.biz/console/channel/$LINE_CHANNEL_ID/messaging-api 獲取: " LINE_CHANNEL_ACCESS_TOKEN
    echo "LINE_CHANNEL_ACCESS_TOKEN=$LINE_CHANNEL_ACCESS_TOKEN" >> .env

    read -p "請輸入 Channel Secret 請至 https://developers.line.biz/console/channel/$LINE_CHANNEL_ID/basics 獲取: " LINE_CHANNEL_SECRET
    echo "LINE_CHANNEL_SECRET=$LINE_CHANNEL_SECRET" >> .env

    read -p "請輸入 ngrok authtoken 請至 https://dashboard.ngrok.com/get-started/your-authtoken 獲取: " NGROK_TOKEN
    echo "NGROK_TOKEN=$NGROK_TOKEN" >> .env

    read -p "請輸入 DISCORD_TOKEN 請至 https://discord.com/developers/applications Bot 選單中獲取:" DISCORD_TOKEN
    echo "DISCORD_TOKEN=$DISCORD_TOKEN" >> .env

    read -p "請輸入 DISCORD_CHANNEL_ID 請至你自己的Discord頻道的文字頻道, 右鍵複製頻道ID: " DISCORD_CHANNEL_ID
    echo "DISCORD_CHANNEL_ID=$DISCORD_CHANNEL_ID" >> .env
fi

# # 從 .env 檔案讀取 NGROK_TOKEN
# source .env

# chmod 777 ./ngrok

# ./ngrok authtoken $NGROK_TOKEN && ./ngrok http https://localhost:8000 || { echo "啟動 ngrok 失敗，請檢查。"; exit 1; }

# echo "請至 https://developers.line.biz/console/channel/$LINE_CHANNEL_ID/messaging-api 設定 Webhook URL"

# 開啟 Discord bot
screen -S discord_bot -dm bash -c "$HOME/miniconda3/bin/python3.11 $HOME/ZR-bot/bot_discord.py"

# 開啟 Line bot
screen -S line_bot -dm bash -c "$HOME/miniconda3/bin/python3.11 $HOME/ZR-bot/bot_line.py"

echo "所有操作完成。"
