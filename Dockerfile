FROM python:3.11.4

# 安裝 git 和 playwright
RUN apt-get update && apt-get install -y git
RUN python -m pip install --upgrade pip
RUN pip install future
RUN pip install playwright
RUN playwright install chromium

# 克隆你的 GitHub 倉庫
RUN git clone https://x-access-token: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@github.com/Mai0313/ZR-bot.git /app

WORKDIR /app

# 安裝其他所需的依賴項
RUN pip install -r requirements.txt

# 使用 CMD 來啟動其中一個機器人，或者使用 ENTRYPOINT 讓你在運行容器時選擇
CMD ["python", "bot_discord.py"]
# CMD ["python", "bot_line.py"]
