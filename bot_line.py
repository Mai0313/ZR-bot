import asyncio

from flask import Flask, abort, request
from flask_apscheduler import APScheduler
from linebot.v3 import WebhookHandler
from linebot.v3.exceptions import InvalidSignatureError
from linebot.v3.messaging import ApiClient, Configuration
from linebot.v3.webhooks import MessageEvent, PostbackEvent, TextMessageContent
from omegaconf import OmegaConf

from cogs.billing_history.get_accounting import AccountingLine
from cogs.billing_history.get_financing import FinanceLine
from cogs.create_billing.get_billing import BillingLine
from cogs.create_permission.get_userid import PermissionGenerator
from cogs.create_permission.permission_control import PermissionLine
from cogs.help.help import ListCommands
from cogs.search_billing.get_billing_info import SearchBillingLine
from environment import LINE_CHANNEL_ACCESS_TOKEN, LINE_CHANNEL_SECRET
from src.line_utils import LineResponse

cfg = OmegaConf.load("configs/setting.yaml")

configuration = Configuration(access_token=LINE_CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(LINE_CHANNEL_SECRET)

app = Flask(__name__)

@app.route('/')
def home():
    return 'Hello, World!'

@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        app.logger.info("Invalid signature. Please check your channel access token/channel secret.")
        abort(200)
    return '', 200

def get_permission(api_client, event):
    permission = OmegaConf.load("configs/permission.yaml")
    if (event.source.user_id not in permission.whitelist.values()) or (event.source.user_id not in permission.admin.values()):
        LineResponse().reply_message(api_client, event, "❌ 您沒有權限使用此機器人")
        return abort(200)

@handler.add(MessageEvent, message=TextMessageContent)
def handle_message(event):
    with ApiClient(configuration) as api_client:
        get_permission(api_client, event)
        if event.message.text.isdigit():
            BillingLine().create_billing(api_client, event)

        elif "命令" in event.message.text and len(event.message.text) == 2:
            ListCommands().list_commands(api_client, event)

        elif "記帳" in event.message.text or "入帳" in event.message.text:
            AccountingLine().accounting(api_client, event)

        elif "CV" in event.message.text and len(event.message.text) == 14:
            SearchBillingLine().search_billing(api_client, event)

        elif "查帳" in event.message.text and len(event.message.text) == 2:
            FinanceLine().check_account(api_client, event)

        elif "id" in event.message.text and len(event.message.text) == 2:
            PermissionGenerator().get_user_id(api_client, event)

        elif "gid" in event.message.text and len(event.message.text) == 3:
            PermissionGenerator().get_chatroom_id(api_client, event)

        elif "新增" in event.message.text and "@" in event.message.text:
            """user id is 33 digits, so 33+5 = 38"""
            PermissionLine().add_permission(api_client, event)

        elif "刪除" in event.message.text and "@" in event.message.text:
            PermissionLine().drop_permission(api_client, event)

        elif "權限" in event.message.text and len(event.message.text) == 2:
            PermissionLine().get_permission_list(api_client, event)

@handler.add(PostbackEvent)
def handle_postback(event):
    with ApiClient(configuration) as api_client:
        FinanceLine().check_account_response(api_client, event)
        # PermissionLine().edit_permission_postback(api_client, event)

scheduler = APScheduler()

class Config:
    SCHEDULER_API_ENABLED = True

@scheduler.task('interval', id='do_job_1', minutes=5, misfire_grace_time=900)
def updater():
    with ApiClient(configuration) as api_client:
        asyncio.run(SearchBillingLine().update_billing_info(api_client))  # 使用 asyncio.run 來調用非同步函數

if __name__ == "__main__":
    app.config.from_object(Config())
    scheduler.init_app(app)
    scheduler.start()
    app.run(port=8000, ssl_context="adhoc", debug=cfg.debug)
