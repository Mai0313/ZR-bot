@echo off
chcp 65001
setlocal enabledelayedexpansion

REM 設定顏色代碼
for /f %%a in ('echo prompt $E ^| cmd') do set "ESC=%%a"
set "RED=%ESC%[91m"
set "GREEN=%ESC%[92m"
set "YELLOW=%ESC%[93m"
set "BLUE=%ESC%[94m"
set "RESET=%ESC%[0m"

echo %GREEN%檢查 Python 是否已安裝...%RESET%
where python > nul 2>&1
if %errorlevel% neq 0 (
    echo %RED%Python 沒有安裝。請從以下連結安裝: https://www.python.org/ftp/python/3.9.13/python-3.9.13-amd64.exe%RESET%
    pause
    exit /b
)

echo %YELLOW%正在安裝 requirements.txt 中的套件...%RESET%
pip install -r requirements.txt > nul 2>&1
if %errorlevel% neq 0 (
    echo %RED%安裝 requirements.txt 失敗%RESET%
    pause
    exit /b
)

echo %YELLOW%正在安裝 Playwright 的 Chromium...%RESET%
playwright install chromium
if %errorlevel% neq 0 (
    echo %RED%安裝 Chromium 失敗%RESET%
    pause
    exit /b
)

echo %GREEN%檢查 .env 文件是否存在...%RESET%
if not exist .env (
    echo %YELLOW%.env 文件不存在，正在創建...%RESET%

    REM 詢問 LINE_CHANNEL_ID
    set /p "LINE_CHANNEL_ID=請輸入 Line Channel ID (純數字) 請至Line Developer -> Basic Settings 獲取: "
    echo LINE_CHANNEL_ID=!LINE_CHANNEL_ID!>>.env

    REM 詢問 LINE_CHANNEL_ACCESS_TOKEN
    set /p "LINE_CHANNEL_ACCESS_TOKEN=請輸入 Channel access token 請至 https://developers.line.biz/console/channel/!LINE_CHANNEL_ID!/messaging-api 獲取: "
    echo LINE_CHANNEL_ACCESS_TOKEN=!LINE_CHANNEL_ACCESS_TOKEN!>>.env

    REM 詢問 LINE_CHANNEL_SECRET
    set /p "LINE_CHANNEL_SECRET=請輸入 Channel Secret 請至 https://developers.line.biz/console/channel/!LINE_CHANNEL_ID!/basics 獲取: "
    echo LINE_CHANNEL_SECRET=!LINE_CHANNEL_SECRET!>>.env

    REM 詢問 NGROK_TOKEN
    set /p "NGROK_TOKEN=請輸入 ngrok authtoken 請至 https://dashboard.ngrok.com/get-started/your-authtoken 獲取: "
    echo NGROK_TOKEN=!NGROK_TOKEN!>>.env

    REM 詢問 DISCORD_TOKEN
    set /p "DISCORD_TOKEN=請輸入 DISCORD_TOKEN 請至 https://discord.com/developers/applications Bot 選單中獲取:"
    echo DISCORD_TOKEN=!DISCORD_TOKEN!>>.env

    REM 詢問 DISCORD_CHANNEL_ID
    set /p "DISCORD_CHANNEL_ID=請輸入 DISCORD_CHANNEL_ID 請至你自己的Discord頻道的文字頻道, 右鍵複製頻道ID: "
    echo DISCORD_CHANNEL_ID=!DISCORD_CHANNEL_ID!>>.env
)

echo %BLUE%env 文件內容如下:%RESET%
type .env
echo.
echo %GREEN%請確認內容無誤，按下任意鍵繼續...%RESET%
pause

REM 從 .env 檔案讀取 NGROK_TOKEN
for /f "tokens=1,* delims==" %%a in ('type .env ^| findstr /i "NGROK_TOKEN="') do (
    set "token=%%b"
)

start powershell -command "./ngrok authtoken %token%; ./ngrok http https://localhost:8000"
if %errorlevel% neq 0 (
    echo %RED%啟動 ngrok 失敗%RESET%
    pause
    exit /b
)

for /f "tokens=1,* delims==" %%a in ('type .env ^| findstr /i "LINE_CHANNEL_ID="') do (
    set "LINE_CHANNEL_ID=%%b"
)

echo 請至 %YELLOW%https://developers.line.biz/console/channel/!LINE_CHANNEL_ID!/messaging-api%RESET% 設定 %YELLOW%Webhook URL%RESET%
echo %GREEN%添加完畢後, 按下任意鍵繼續...%RESET%
pause

REM 開啟 Line bot
start powershell -command "python bot_line.py"
if %errorlevel% neq 0 (
    echo %RED%啟動 bot_line.py 失敗%RESET%
    pause
    exit /b
)

REM 開啟 Discord bot
start powershell -command "python bot_discord.py"
if %errorlevel% neq 0 (
    echo %RED%啟動 bot_discord.py 失敗%RESET%
    pause
    exit /b
)

echo 操作完成，其他三個視窗請保持開啟。
pause
