import discord
from discord.ext import commands
from omegaconf import OmegaConf

from cogs.billing_history.get_accounting import AccountingCog
from cogs.billing_history.get_financing import FinanceCog
from cogs.create_billing.get_billing import BillingCog
from cogs.search_billing.get_billing_info import SearchBillingCog
from environment import DISCORD_TOKEN

cfg = OmegaConf.load("configs/setting.yaml")

intents = discord.Intents.all()
intents.messages = True

bot = commands.Bot(command_prefix='//', intents=intents)

@bot.event
async def on_ready():
    await bot.wait_until_ready() # 等待 bot 準備就緒
    await bot.add_cog(BillingCog(bot))
    await bot.add_cog(SearchBillingCog(bot))
    await bot.add_cog(AccountingCog(bot))
    await bot.add_cog(FinanceCog(bot))

@bot.command(name='cogs')
async def list_cogs(ctx):
    cogs = ', '.join(bot.cogs.keys())
    message = f"""```{cogs}```"""
    await ctx.send(f'Loaded cogs: {message}')


bot.run(token = DISCORD_TOKEN, reconnect=True)
