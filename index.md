# ZR-Bot

## Installation

- `start.bat`
  - 會一步一步引導你完成機器人設定

## Required Files / Configuration

- `.env`

  - 這個檔案紀錄了所有token，切記這個檔案不要公開

- `./configs/permission.yaml`

  - 這個檔案不會被git追蹤，如果沒有，請參考 `./configs/permission.yaml.example` 來建立

## Command List

### Line Bot

- `命令`: 列出所有指令

- `記帳`: 可接受數字 正負號也計算；也可以接受訂單序號

  - `記帳500`: 收入500元
  - `記帳-500`: 支出500元
  - `記帳CVS30818426953`: 自動查找對應單號填入對應金額

- `查詢訂單`: 直接將繳費代碼貼上即可

  > ![Alt text](img/search_payment.png)

- `查帳`: 查詢歷史交易紀錄

  > ![Alt text](img/search_history.png)

- `id/gid`: 查詢使用者ID/群組ID

  > ![Alt text](img/search_id.png)

- `新增/刪除`: 支持新增/刪除 管理員、使用者與通知

  - 管理員:
    - `新增管理員@user_id@user_name`
      - `user_id`: 請用`id/gid`指令查詢
      - `user_name`: 可以自訂一個名稱，以你方便好用為主
  - 使用者:
    - `新增使用者@user_id@user_name`
      - `user_id`: 請用`id/gid`指令查詢
      - `user_name`: 可以自訂一個名稱，以你方便好用為主
  - 通知:
    - 支援`group id`與`user id`；換言之就是你可以選擇要私訊通知你、或是群組通知你
    - `新增通知@user_id@user_name` or `新增通知@group_id@group_name`
      - `user_id`: 請用`id/gid`指令查詢
      - `group_id`: 請用`id/gid`指令查詢
      - `user_name`: 可以自訂一個名稱，以你方便好用為主

  以上指令只要把`新增`改成`刪除`即可

  #### 切記，刪除時只需要給當初設定的user name即可；忘了user name可以參考下面一個指令

  #### 通知可以是一人以上，只需要重複新增即可

  > ![Alt text](img/multi_group_notify.png)

- `權限`: 如果你忘記你設定的權限有誰了，可以使用這個指令

  > ![Alt text](img/permission.png)

### Discord Bot

- 待補充，請自行發掘

### License
