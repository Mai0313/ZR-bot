import datetime
import io

import discord
from discord.ext import commands
from linebot.v3.messaging.models import ButtonsTemplate, MessageAction, TemplateMessage
from omegaconf import OmegaConf

from environment import PAYMENT_FEE
from src.datamodule import DatabaseRecorder
from src.get_payment_code import PaymentDetailsFetcher
from src.get_payment_line import PaymentDetailsFetcherSync
from src.line_utils import LineResponse
from src.utils.embed_message import CreateMessage
from src.utils.get_price import get_price_to_int

cfg = OmegaConf.load("configs/setting.yaml")

class BillingCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='開單')
    async def create_billing(self, ctx):
        content = ctx.message.content
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        message = ctx.message
        initial_message = await ctx.send(cfg.messages.on_payment_command_recieved)
        status, price = get_price_to_int(content)
        if not status:
            embed_msg = CreateMessage().edit_embed_message(cfg.messages.on_input_error, cfg.messages.on_no_input_error.format(your_input=price), status=False)
            await initial_message.edit(embed=embed_msg)
            return
        if price < 2:
            embed_msg = CreateMessage().edit_embed_message(cfg.messages.on_input_error, cfg.messages.on_underpaid_error, status=False)
            await initial_message.edit(embed=embed_msg)
            return
        price = price + PAYMENT_FEE
        fetcher = PaymentDetailsFetcher()
        await fetcher.start_browser()
        payment_code, payment_amount, payment_date, order_id, screenshot = await fetcher.get_payment_details(price)
        response = cfg.messages.on_payment_created.format(payment_code=payment_code, payment_amount=payment_amount)
        await ctx.send(content=f"{response}", allowed_mentions=discord.AllowedMentions.none())
        qrcode = discord.File(io.BytesIO(screenshot), filename="image.png")
        await ctx.send(file=qrcode)
        await initial_message.delete()
        DatabaseRecorder().get_history(current_time, payment_code, payment_amount, payment_date, order_id)
        await fetcher.close_browser()


class BillingLine:
    def __init__(self):
        pass

    def create_billing(self, api_client, event):
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        status, price = get_price_to_int(event.message.text)
        if not status:
            LineResponse().reply_message(api_client, event, "請輸入價格")
            return None
        if price < 2:
            LineResponse().reply_message(api_client, event, "價格必須大於30元")
            return None

        price += PAYMENT_FEE
        fetcher = PaymentDetailsFetcherSync()
        fetcher.start_browser()
        payment_code, payment_amount, payment_date, order_id, screenshot = fetcher.get_payment_details(price)
        response = cfg.messages.on_payment_created.format(payment_code=payment_code, payment_amount=payment_amount)
        response = response.replace("<", "").replace(">", "")
        tmp = f"""
繳費代碼: {payment_code}
開單時間: {current_time[:-9]}
繳費期限: {payment_date[:-9]}
"""
        buttons_template = ButtonsTemplate(
            title = f"✅開單成功通知\n新台幣 {payment_amount.replace('NT', '').replace('$', '')}",
            text=tmp,
            thumbnail_image_url=cfg.image.image_url,
            image_size='cover',
            actions=[
                MessageAction(label="複製繳費代碼", text=payment_code + " "),
                MessageAction(label="複製繳費連結", text=response),
                MessageAction(label="複製QR Code", text="笑死根本沒有"),
            ]
        )
        template_message = TemplateMessage(alt_text=cfg.messages.on_payment_search_notify, template=buttons_template)
        # LineResponse().reply_option_message(api_client, event, template_message)
        LineResponse().reply_message(api_client, event, response)
        # LineResponse().send_notify_message("\nQR Code:", screenshot)
        DatabaseRecorder().get_history(current_time, payment_code, payment_amount, payment_date, order_id)
