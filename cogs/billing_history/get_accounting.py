import datetime

from discord.ext import commands
from omegaconf import OmegaConf

from src.datamodule import DatabaseRecorder
from src.line_utils import LineResponse
from src.utils.embed_message import CreateMessage
from src.utils.get_price import get_price_to_int

cfg = OmegaConf.load("configs/setting.yaml")

class AccountingCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='記帳')
    async def accounting(self, ctx, *, content: str):
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        initial_message = await ctx.send(cfg.messages.on_accounting_command_recieved)
        if "CV" in content:
            payment_code = content.split("CV")[-1]
            payment_code = f"CV{payment_code}"
            payment_code, amount, created_time, status = DatabaseRecorder().get_accounting_from_payment_code(payment_code)
            if status:
                extra = cfg.messages.on_saving_success_content.format(payment_code=payment_code, amount=amount, created_time=created_time)
                embed_msg = CreateMessage().edit_embed_message(cfg.messages.on_saving_success, extra, status)
                await initial_message.edit(embed=embed_msg)
            elif status:
                embed_msg = CreateMessage().edit_embed_message(cfg.messages.on_saving_success, cfg.messages.on_saving_error_content, status)
                await initial_message.edit(embed=embed_msg)
            return
        else:
            status, amount = get_price_to_int(content)
            DatabaseRecorder().get_accounting(current_time, amount)
            extra = cfg.messages.on_accounting_success_content.format(amount=amount, created_time=current_time)
            embed_msg = CreateMessage().edit_embed_message(cfg.messages.on_accounting_success, extra, status)
            await initial_message.edit(embed=embed_msg)
            return

class AccountingLine:
    def __init__(self):
        pass

    def accounting(self, api_client, event):
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if "CV" in event.message.text:
            payment_code = event.message.text.split("CV")[-1]
            payment_code = f"CV{payment_code}"
            payment_code, amount, created_time, status = DatabaseRecorder().get_accounting_from_payment_code(payment_code)
            if status:
                extra = cfg.messages.on_saving_success_content.format(payment_code = payment_code, amount = amount, created_time = created_time)
                LineResponse().reply_message(api_client, event, extra)
            elif status:
                LineResponse().reply_message(api_client, event, cfg.messages.on_saving_error_content)
            return
        else:
            status, amount = get_price_to_int(event.message.text)
            DatabaseRecorder().get_accounting(current_time, amount)
            extra = cfg.messages.on_accounting_success_content.format(amount = amount, created_time = current_time)
            LineResponse().reply_message(api_client, event, extra)
            return
