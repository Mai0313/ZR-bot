import discord
import pandas as pd
from discord.ext import commands
from linebot.v3.messaging.models import (
    ButtonsTemplate,
    PostbackAction,
    TemplateMessage,
)
from omegaconf import OmegaConf

from src.datamodule.database import Database
from src.line_utils import LineResponse
from src.utils.embed_message import CreateMessage

cfg = OmegaConf.load("configs/setting.yaml")

class FinanceCog(commands.Cog):
    """這個只是簡單的查帳,不會記帳,只會列出所有帳務資訊."""
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='查帳')
    async def check_account(self, ctx):
        embed = discord.Embed(
            title=cfg.finance.title,
            description=cfg.finance.title_content,
        )
        msg = await ctx.send(embed=embed)
        for emoji in ('🥇', '🥈', '🥉'):
            await msg.add_reaction(emoji)

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        if user.bot:
            return

        data = Database().get_data_from_db("payment")

        if reaction.emoji == '🥇':
            data = data.iloc[-1:, :]
            total_income = data["付款金額"].sum()
            message_description = cfg.finance.all_payment_info.format(total_numbers=len(data), total_income=total_income, data=data)
            embed_msg = CreateMessage().edit_embed_message(cfg.finance.last_payment, message_description, True)
            await reaction.message.channel.send(embed=embed_msg)

        elif reaction.emoji == '🥈':
            data = data.iloc[-5:, :]
            total_income = data["付款金額"].sum()
            message_description = cfg.finance.all_payment_info.format(total_numbers=len(data), total_income=total_income, data=data)
            embed_msg = CreateMessage().edit_embed_message(cfg.finance.last_five_payment, message_description, True)
            await reaction.message.channel.send(embed=embed_msg)

        elif reaction.emoji == '🥉':
            data = data
            total_income = data["付款金額"].sum()
            message_description = cfg.finance.all_payment_info.format(total_numbers=len(data), total_income=total_income, data=data)
            embed_msg = CreateMessage().edit_embed_message(cfg.finance.all_payment_of_the_month, message_description, True)
            await reaction.message.channel.send(embed=embed_msg)

class FinanceLine:
    def __init__(self):
        pass

    def check_account(self, api_client, event):
        buttons_template = ButtonsTemplate(
            title=cfg.finance.title,
            text=cfg.finance.title_content,
            thumbnail_image_url=cfg.image.image_url,
            image_size='cover',
            actions=[
                PostbackAction(label=cfg.finance.last_payment, data=cfg.finance.last_payment),
                PostbackAction(label=cfg.finance.last_five_payment, data=cfg.finance.last_five_payment),
                PostbackAction(label=cfg.finance.all_payment_of_the_month, data=cfg.finance.all_payment_of_the_month)
                # MessageAction(label=cfg.finance.all_payment_of_the_month, text=cfg.finance.all_payment_of_the_month),
                # URIAction(label='打開網站', uri='https://www.example.com')
            ]
        )
        template_message = TemplateMessage(alt_text='選項圖文消息', template=buttons_template)
        LineResponse().reply_option_message(api_client, event, template_message)

    def check_account_response(self, api_client, event):
        data = Database().get_data_from_db("payment")
        if event.postback.data == cfg.finance.last_payment:
            data = data.iloc[-1:, :]
        elif event.postback.data == cfg.finance.last_five_payment:
            data = data.iloc[-5:, :]
        elif event.postback.data == cfg.finance.all_payment_of_the_month:
            data = data
        else:
            data = pd.DataFrame()

        if not data.empty:
            data['訂單建立日期'] = pd.to_datetime(data['訂單建立日期'])
            data['月份'] = data['訂單建立日期'].dt.month
            grouped_by_month = data.groupby('月份')

            contents = {
                "type": "carousel",
                "contents": []
            }
            for month, group in grouped_by_month:
                bubble_content = {
                    "type": "bubble",
                    "size": "mega",
                    "header": {
                        "type": "box",
                        "layout": "vertical",
                        "backgroundColor": "#f0f4f7",
                        "contents": [
                            {
                                "type": "text",
                                "text": f"{month}月歷史訂單",
                                "size": "lg",
                                "weight": "bold",
                                "align": "center",
                            },
                            # {
                            #     "type": "image",
                            #     "url": cfg.image.image_url,
                            #     "align": "center",
                            #     "size": "full",
                            #     "aspectRatio": "1:1",
                            # },
                        ],
                    },
                    "body": {
                        "type": "box",
                        "layout": "vertical",
                        "backgroundColor": "#f0f4f7",
                        "contents": [
                            {
                                "type": "box",
                                "layout": "horizontal",
                                "contents": [
                                    {"type": "text", "text": "訂單日期", "weight": "bold", "align": "center", "size": "sm"},
                                    {"type": "text", "text": "付款代碼", "weight": "bold", "align": "center", "size": "sm"},
                                    {"type": "text", "text": "付款金額", "weight": "bold", "align": "center", "size": "sm"},
                                    {"type": "text", "text": "交易狀態", "weight": "bold", "align": "center", "size": "sm"},
                                ]
                            },
                            {"type": "separator", "color": "#696969", "margin": "sm"},
                        ],
                        "paddingStart": "0px",
                        "paddingEnd": "0px"
                    }
                }

                total_amount = 0
                for index, row in group.iterrows():
                    date, time = str(row['訂單建立日期']).split(' ')
                    data_box = {
                        "type": "box",
                        "layout": "horizontal",
                        "contents": [
                            {"type": "text", "text": date + "\n" + time, "wrap": True, "size": "xxs", "align": "center", "weight": "bold"},
                            {"type": "text", "text": str(row['付款代碼'])[:7] + "\n" + str(row['付款代碼'])[7:14], "wrap": True, "size": "xxs", "align": "center", "weight": "bold"},
                            {"type": "text", "text": str(row['付款金額']), "size": "xxs", "align": "center", "weight": "bold"},
                            {"type": "text", "text": str(row['交易狀態']), "size": "xxs", "align": "center", "weight": "bold"},
                        ],
                        "justifyContent": "space-between",
                        "spacing": "md",
                        "margin": "none",
                        "height": "50px",
                    }

                    bubble_content['body']['contents'].append(data_box)
                    total_amount += float(str(row['付款金額']).replace(",", "").replace("NT$", "").replace("元", ""))

                    total_amount_box = {
                        "type": "box",
                        "layout": "horizontal",
                        "contents": [
                            {"type": "text", "text": "總金額", "size": "sm", "align": "center", "weight": "bold"},
                            {"type": "text", "text": str(total_amount), "size": "sm", "align": "center", "weight": "bold"},
                        ],
                        "spacing": "md",
                        "margin": "none",
                        "height": "35px",
                    }
                bubble_content['body']['contents'].append(total_amount_box)
                contents['contents'].append(bubble_content)
            LineResponse().reply_flex_message(api_client, event, contents, custom_alt_text = "歷史交易查詢")
            # LineResponse().reply_message(api_client, event, cfg.finance.all_payment_info.format(total_numbers=len(data), total_income=total_amount, data=data))

            # contents = {
            #     "type": "carousel",
            #     "contents": []
            # }

            # for index, row in data.iterrows():
            #     # 創建一個包含標題行的 bubble
            #     bubble_content = {
            #         "type": "bubble",
            #         "size": "mega",
            #         "body": {
            #             "type": "box",
            #             "layout": "vertical",
            #             "contents": [
            #                 {
            #                     "type": "box",
            #                     "layout": "horizontal",
            #                     "contents": [
            #                         {"type": "text", "text": "訂單建立日期", "weight": "bold", "align": "center", "size": "sm"},
            #                         {"type": "text", "text": "付款代碼", "weight": "bold", "align": "center", "size": "sm"},
            #                         {"type": "text", "text": "付款金額", "weight": "bold", "align": "center", "size": "sm"},
            #                         {"type": "text", "text": "交易狀態", "weight": "bold", "align": "center", "size": "sm"},
            #                     ]
            #                 },
            #                 {"type": "separator", "color": "#696969", "margin": "sm"},
            #             ],
            #             "paddingStart": "0px",
            #             "paddingEnd": "0px"
            #         }
            #     }

            #     data_box = {
            #         "type": "box",
            #         "layout": "horizontal",
            #         "contents": [
            #             {"type": "text", "text": str(row['訂單建立日期']), "size": "xxs", "align": "center", "weight": "bold"},
            #             {"type": "text", "text": str(row['付款代碼']), "size": "xxs", "align": "center", "weight": "bold"},
            #             {"type": "text", "text": str(row['付款金額']), "size": "xxs", "align": "center", "weight": "bold"},
            #             {"type": "text", "text": str(row['交易狀態']), "size": "xxs", "align": "center", "weight": "bold"},
            #         ],
            #         "justifyContent": "space-between",
            #         "spacing": "md",
            #         "margin": "none",
            #         "height": "35px",
            #     }

            #     # 將數據盒子添加到 bubble 的主體中
            #     bubble_content['body']['contents'].append(data_box)
            #     # 將 bubble 添加到 carousel 的內容中
            #     contents['contents'].append(bubble_content)
