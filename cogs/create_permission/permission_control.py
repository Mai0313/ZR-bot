import datetime

import autorootcwd  # noqa: F401
import pandas as pd
from omegaconf import OmegaConf

from src.line_utils import LayoutGenerator, LineResponse

cfg = OmegaConf.load("configs/setting.yaml")
permission = OmegaConf.load("configs/permission.yaml")

class PermissionUpdate:
    def __init__(self):
        pass

    def update_whitelist(self, username, userid):
        # if len(userid) != 33:
        #     return "更新失敗, user id必須為33碼"
        # elif len(userid) == 33:
        permission = OmegaConf.load("configs/permission.yaml")
        whitelist = permission.whitelist
        whitelist.update({username: userid})
        OmegaConf.save(permission, "configs/permission.yaml")
        return f"""已新增使用者: {username}"""

    def delete_whitelist(self, username):
        permission = OmegaConf.load("configs/permission.yaml")
        del permission.whitelist[username]
        OmegaConf.save(permission, "configs/permission.yaml")
        return f"""已刪除使用者: {username}"""

    def update_admin(self, username, userid):
        permission = OmegaConf.load("configs/permission.yaml")
        admin = permission.admin
        admin.update({username: userid})
        OmegaConf.save(permission, "configs/permission.yaml")
        return f"""已新增管理員: {username}"""

    def delete_admin(self, username):
        permission = OmegaConf.load("configs/permission.yaml")
        del permission.admin[username]
        OmegaConf.save(permission, "configs/permission.yaml")
        return f"""已刪除管理員: {username}"""

    def update_notify(self, username, userid):
        permission = OmegaConf.load("configs/permission.yaml")
        whitelist = permission.Notify
        whitelist.update({username: userid})
        OmegaConf.save(permission, "configs/permission.yaml")
        return f"""已新增使用者: {username}"""

    def delete_notify(self, username):
        permission = OmegaConf.load("configs/permission.yaml")
        del permission.Notify[username]
        OmegaConf.save(permission, "configs/permission.yaml")
        return f"""已刪除使用者: {username}"""


class PermissionLine:
    def __init__(self):
        self.user_states = {}

    def add_permission(self, api_client, event):
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        user_response = event.message.text.split("@")
        user_command = user_response[0]
        userid = user_response[1]
        username = user_response[2]
        if event.source.user_id not in permission.admin.values():
            LineResponse().reply_message(api_client, event, "你沒有權限")
            return
        else:
            if user_command == "新增使用者":
                per = PermissionUpdate().update_whitelist(username, userid)
            elif user_command == "新增管理員":
                per = PermissionUpdate().update_admin(username, userid)
            elif user_command == "新增通知":
                per = PermissionUpdate().update_notify(username, userid)
            LineResponse().reply_message(api_client, event, per)

    def drop_permission(self, api_client, event):
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        user_response = event.message.text.split("@")
        user_command = user_response[0]
        username = user_response[1]
        if event.source.user_id not in permission.admin.values():
            LineResponse().reply_message(api_client, event, "你沒有權限")
            return
        else:
            if user_command == "刪除使用者":
                per = PermissionUpdate().delete_whitelist(username)
            elif user_command == "刪除管理員":
                per = PermissionUpdate().delete_admin(username)
            elif user_command == "刪除通知":
                per = PermissionUpdate().delete_notify(username)
            LineResponse().reply_message(api_client, event, per)

    def get_permission_list(self, api_client, event):
        permission = OmegaConf.load("configs/permission.yaml")
        admin = pd.DataFrame(permission.admin.items(), columns = ["使用者名稱", "使用者ID"])
        admin["權限"] = "管理員"
        whitelist = pd.DataFrame(permission.whitelist.items(), columns = ["使用者名稱", "使用者ID"])
        whitelist["權限"] = "使用者"
        result = pd.concat([admin, whitelist], axis=0)
        permission_group = result.groupby(["權限"])
        fields = [
            {"title": "使用者名稱", "key": "使用者名稱"},
            {"title": "權限", "key": "權限"}
        ]
        contents = LayoutGenerator.create_carousel(permission_group, "{}名單", fields)
        LineResponse().reply_flex_message(api_client, event, contents, custom_alt_text = "權限查詢")

    # def edit_permission(self, api_client, event):
    #     current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    #     if event.source.user_id not in permission.admin:
    #         LineResponse().reply_message(api_client, event, "你沒有權限")
    #         return None
    #     else:
    #         self.user_states[event.source.user_id] = {}
    #         tmp = "請選擇你要編輯的項目"
    #         buttons_template = ButtonsTemplate(
    #             title="🛠️權限編輯",
    #             text=tmp,
    #             thumbnail_image_url=cfg.image.image_url,
    #             image_size='cover',
    #             actions=[
    #                 PostbackAction(label="新增管理員", data="新增管理員"),
    #                 PostbackAction(label="移除管理員", data="移除管理員"),
    #                 PostbackAction(label="檢視現有權限", data="檢視現有管理員"),
    #             ]
    #         )
    #         template_message = TemplateMessage(alt_text="權限編輯", template=buttons_template)
    #         LineResponse().reply_option_message(api_client, event, template_message)

    # def edit_permission_postback(self, api_client, event):
    #     user_id = event.source.user_id
    #     state = self.user_states.get(user_id, {})

    #     state['timestamp'] = datetime.datetime.now()

    #     if event.postback.data == "新增管理員":
    #         state['step'] = 'enter_admin_id'
    #         tmp = "請輸入要新增的管理員ID"
    #         LineResponse().reply_message(api_client, event, tmp)
    #     elif event.postback.data == "移除管理員":
    #         state['step'] = 'remove_admin'
    #         tmp = "請輸入要刪除的管理員名稱"
    #         LineResponse().reply_message(api_client, event, tmp)
    #     elif event.postback.data == "檢視現有管理員":
    #         tmp = "現有管理員ID如下:\n"
    #         with open("permission.json", encoding="utf-8") as f:
    #             permission = json.load(f)
    #         for i in permission.keys():
    #             tmp += f"{i}\n"
    #         LineResponse().reply_message(api_client, event, tmp)
    #     else:
    #         pass

    #     self.user_states[user_id] = state

    # def handle_user_input(self, api_client, event):
    #     user_id = event.source.user_id
    #     user_input = event.message.text
    #     state = self.user_states.get(user_id, {})

    #     timestamp = state.get('timestamp')
    #     if timestamp and (datetime.datetime.now() - timestamp).total_seconds() > 10:
    #         self.user_states.pop(user_id, None)
    #         return
    #     if state.get('step') == 'enter_admin_id':
    #         state['admin_id'] = user_input
    #         state['step'] = 'enter_admin_name'
    #         LineResponse().reply_message(api_client, event, "請輸入管理員名稱")
    #     elif state.get('step') == 'enter_admin_name':
    #         state['admin_name'] = user_input
    #         PermissionUpdate().update_permission(state['admin_name'], state['admin_id'])
    #         self.user_states.pop(user_id, None)
    #         LineResponse().reply_message(api_client, event, f"🎉 已新增管理員: \n {user_input}")
    #     elif state.get('step') == 'remove_admin':
    #         state['admin_name'] = user_input
    #         try:
    #             PermissionUpdate().delete_permission(state['admin_name'])
    #         except KeyError:
    #             tmp = "現有管理員ID如下:\n"
    #             with open("permission.json", encoding="utf-8") as f:
    #                 permission = json.load(f)
    #             for i in permission.keys():
    #                 tmp += f"{i}\n"
    #             LineResponse().reply_message(api_client, event, f"此管理員不存在, {tmp}")
    #             return None
    #         self.user_states.pop(user_id, None)
    #         LineResponse().reply_message(api_client, event, f"🎉 已刪除管理員: \n {user_input}")
    #     else:
    #         pass

    #     self.user_states[user_id] = state
