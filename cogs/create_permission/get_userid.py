from src.line_utils import LineResponse


class PermissionGenerator:
    def __init__(self):
        pass

    def get_user_id(self, api_client, event):
        message = event.source.user_id
        LineResponse().reply_message(api_client, event, message)

    def get_chatroom_id(self, api_client, event):
        message = event.source.group_id
        LineResponse().reply_message(api_client, event, message)
