import datetime

import pandas as pd
from discord.ext import commands, tasks
from omegaconf import OmegaConf

from environment import DISCORD_CHANNEL_ID
from src.datamodule.database import Database
from src.line_utils import LineResponse
from src.line_utils.layout import LayoutGenerator
from src.search_payment import NewebpayCrawler, NewebpayCrawlerSync
from src.utils.embed_message import CreateMessage
from src.utils.get_price import get_pure_order_id

cfg = OmegaConf.load("configs/setting.yaml")
permission = OmegaConf.load("configs/permission.yaml")

class SearchBillingCog(commands.Cog):
    """There is two main function that allows user to search the billing status and update the billing status automatically (every 5 mins).

    Args:
        None

    Returns:
        None

    Description:
        This function is using async version of the crawler;
        in the meantime, notify or not is based on the `payment_notify_discord` instead of the payment status.

    """
    def __init__(self, bot):
        self.bot = bot
        self.update_billing_info.start()

    @tasks.loop(minutes=5)
    async def update_billing_info(self):
        """This function will run automatically every 5 mins.

        Args:
            None

        Returns:
            None

        Description:
            Logic:
                從數據庫取得數去 > 篩選出未通知的訂單 > 確認訂單是否過期 > 如果過期,將交易狀態改為 `已過期` 、同時將通知狀態也改為 `已通知` >
                > 篩選出未付款訂單 > 丟進去爬蟲用迴圈確認所有訂單是否已繳費 > 如果已繳費,將更改訂單明細 (詳見代碼) > 通知付款訊息 > 將通知狀態改為 `已通知`

        """
        data = Database().get_data_from_db("payment")
        data['訂單建立日期'] = pd.to_datetime(data['訂單建立日期'])
        unpaid_data = data.query("payment_notify_discord == '未通知'")
        current_date = datetime.datetime.now()
        one_month_ago = current_date - datetime.timedelta(days=30)

        if not unpaid_data.empty:
            fetcher = NewebpayCrawler()
            await fetcher.start_browser()
            for index, row in unpaid_data.iterrows():
                order_date = row['訂單建立日期']
                if order_date < one_month_ago:  # 如果訂單建立日期超過一個月
                    data.loc[index, "交易狀態"] = "已過期"  # 將交易狀態更改為已過期
                    data.loc[index, "payment_notify_discord"] = "已通知"  # 將通知狀態更改為已通知,雖文不達意,但是這樣就不會再通知一次了
                    continue

                if row['payment_notify_discord'] == "未通知":
                    order_id = str(row['藍新訂單編號'])
                    email = str(row['電子信箱'])
                    created_time = order_date.strftime("%Y-%m-%d %H:%M:%S")
                    payment_code = row['付款代碼']
                    order_id, payment_status, payment_date, payment_amount, payment_description = await fetcher.get_transaction_details(order_id, email)
                    if payment_status == "已付款":
                        data.loc[index, "交易狀態"] = payment_status
                        data.loc[index, "付款日期"] = payment_date
                        data.loc[index, "付款金額"] = f"NT${payment_amount}元"
                        # 通知付款訊息
                        channel_id = int(DISCORD_CHANNEL_ID)
                        channel = self.bot.get_channel(channel_id)
                        message_content = cfg.discord_related.on_payment_search_notify_content.format(
                            payment_code = payment_code,
                            payment_date = payment_date,
                            created_time = created_time,
                            order_id=order_id,
                            payment_status=payment_status,
                            payment_amount=payment_amount
                        )
                        embed_msg = CreateMessage().edit_embed_message(cfg.messages.on_payment_search_notify, message_content, True)
                        await channel.send(embed=embed_msg)
                        data.loc[index, 'payment_notify_discord'] = '已通知'

            Database().save_data_to_db(data, "payment", "replace")
            await fetcher.close_browser()


    @update_billing_info.before_loop
    async def before_update_billing_info(self):
        await self.bot.wait_until_ready()

    @commands.command(pass_context=True, name='查單')
    async def search_billing(self, ctx, *, content: str):
        initial_message = await ctx.send(cfg.messages.on_payment_search_recieved)
        status, order_id = get_pure_order_id(content)
        if "CV" in order_id:
            data = Database().get_data_from_db("payment")
            data = data.query("付款代碼 == @order_id")
            data['訂單建立日期'] = pd.to_datetime(data['訂單建立日期'])
            order_id = str(data["藍新訂單編號"].values.tolist()[0])
            payment_code = str(data["付款代碼"].values.tolist()[0])
            email = str(data["電子信箱"].values.tolist()[0])
            created_time = str(data["訂單建立日期"].values.tolist()[0])
            created_time_obj = data['訂單建立日期'].iloc[0].to_pydatetime()
            created_time = created_time_obj.strftime('%Y-%m-%d %H:%M:%S')
        fetcher = NewebpayCrawler()
        await fetcher.start_browser()
        order_id, payment_status, payment_date, payment_amount, payment_description = await fetcher.get_transaction_details(order_id, email)
        if payment_status == "未付款":
            status = False
            message_title = cfg.messages.on_payment_search_success
            message_content = cfg.discord_related.on_payment_search_notify_content.format(
                payment_code = payment_code,
                payment_date = payment_date,
                created_time = created_time,
                order_id=order_id,
                payment_status=payment_status,
                payment_amount=payment_amount
            )
        elif not payment_status:
            status = False
            message_title = cfg.messages.on_payment_search_fail
            message_content = cfg.discord_related.on_payment_search_fail_content.format(payment_code=payment_code)
        embed_msg = CreateMessage().edit_embed_message(message_title, message_content, status)
        await initial_message.edit(embed=embed_msg)
        await fetcher.close_browser()

class SearchBillingLine:
    """There is two main function that allows user to search the billing status and update the billing status automatically (every 5 mins).

    Args:
        None


    Returns:
        None

    Description:
        This function is using sync version of the crawler;
        in the meantime, notify or not is based on the `payment_notify_line` instead of the payment status.
    """
    def __init__(self):
        pass

    async def update_billing_info(self, api_client):
        """This function will run automatically every 5 mins.

        Args:
            api_client: line api client

        Returns:
            None

        Description:
            Logic:
                從數據庫取得數去 > 篩選出未通知的訂單 > 確認訂單是否過期 > 如果過期,將交易狀態改為 `已過期` 、同時將通知狀態也改為 `已通知` >
                > 篩選出未付款訂單 > 丟進去爬蟲用迴圈確認所有訂單是否已繳費 > 如果已繳費,將更改訂單明細 (詳見代碼) > 通知付款訊息 > 將通知狀態改為 `已通知`

        """
        data = Database().get_data_from_db("payment")
        data['訂單建立日期'] = pd.to_datetime(data['訂單建立日期'])
        unpaid_data = data.query("payment_notify_line == '未通知'")

        # 確認過期訂單
        current_date = datetime.datetime.now()
        one_month_ago = current_date - datetime.timedelta(days=30)

        if not unpaid_data.empty:
            fetcher = NewebpayCrawler()
            await fetcher.start_browser()
            for index, row in unpaid_data.iterrows():
                order_date = row['訂單建立日期']
                if order_date < one_month_ago:  # 如果訂單建立日期超過一個月
                    data.loc[index, "交易狀態"] = "已過期"  # 將交易狀態更改為已過期
                    data.loc[index, "payment_notify_line"] = "已通知"  # 將通知狀態改為已通知
                    continue
                if row['payment_notify_line'] == "未通知":
                    order_id = str(row['藍新訂單編號'])
                    email = str(row['電子信箱'])
                    order_id, payment_status, payment_date, payment_amount, payment_description = await fetcher.get_transaction_details(order_id, email)
                    payment_amount = f" NT${payment_amount}元"
                    if payment_status == "已付款":
                        data.loc[index, "交易狀態"] = payment_status
                        data.loc[index, "付款日期"] = payment_date
                        data.loc[index, "付款金額"] = f"NT${payment_amount}元"
                        payment_code = row['付款代碼']
                        created_time = row['訂單建立日期']
                        payment_title = cfg.messages.on_payment_search_notify
                        payment_title_color = "#06a820"
                        payment_status = payment_status + " 🟢"
                        flex_contents = LayoutGenerator().create_flex_bubble(cfg, payment_title, payment_title_color, payment_code, payment_date, created_time, order_id, payment_status, payment_amount)
                        for update_notify_user in permission.Notify.values():
                            LineResponse().push_flex_message(api_client, update_notify_user, flex_contents, custom_alt_text = "繳費成功通知")
                        data.loc[index, 'payment_notify_line'] = '已通知'

            Database().save_data_to_db(data, "payment", "replace")
            await fetcher.close_browser()

    def search_billing(self, api_client, event):
        content = event.message.text
        status, order_id = get_pure_order_id(content)
        if "CV" in order_id:
            data = Database().get_data_from_db("payment")
            data = data.query("付款代碼 == @order_id")
            data['訂單建立日期'] = pd.to_datetime(data['訂單建立日期'])
            order_id = str(data["藍新訂單編號"].values.tolist()[0])
            payment_code = str(data["付款代碼"].values.tolist()[0])
            email = str(data["電子信箱"].values.tolist()[0])
            created_time_obj = data['訂單建立日期'].iloc[0].to_pydatetime()
            created_time = created_time_obj.strftime('%Y-%m-%d %H:%M:%S')
        fetcher = NewebpayCrawlerSync()
        fetcher.start_browser()
        order_id, payment_status, payment_date, payment_amount, payment_description = fetcher.get_transaction_details(order_id, email)
        payment_amount = f" NT${payment_amount}元"
        if payment_status == "已付款":
            payment_title = cfg.messages.on_payment_search_notify
            payment_title_color = "#06a820"
            payment_status = payment_status + " 🟢"
        else:
            try:
                payment_title = "尚未繳款"
                payment_title_color = "#FF0000"
                payment_status = payment_status + " 🔴"
            except Exception as e:
                payment_status = "訂單尚未進入藍新系統" + " 🔴"  # noqa: ISC003

        flex_contents = LayoutGenerator().create_flex_bubble(cfg, payment_title, payment_title_color, payment_code, payment_date, created_time, order_id, payment_status, payment_amount)
        LineResponse().reply_flex_message(api_client, event, flex_contents, custom_alt_text = "繳費成功通知")
        fetcher.close_browser()
