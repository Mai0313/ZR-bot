from src.line_utils import LineResponse


class ListCommands:
    def __init__(self):
        pass

    def list_commands(self, api_client, event):
        result = """以下為目前可用的指令:
1. 查帳: 顯示所有訂單紀錄
2. CV: 查詢訂單狀態 (CV為訂單號開頭兩碼)
3. id: 顯示 user id
4. gid: 顯示 group id
5. 新增:
        新增使用者
        新增管理員
            e.g. 新增管理員@user_id@user_name
            注意: user_id只接受33位的ID,有錯一位都會無法新增
6. 刪除:
        刪除使用者
        刪除管理員
            e.g. 刪除管理員@user_name
"""

        LineResponse().reply_message(api_client, event, result)
