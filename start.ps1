chcp 936

Write-Host "干Checking if Python is installed..." -ForegroundColor Green
if (-Not (Get-Command "python" -ErrorAction SilentlyContinue)) {
    Write-Host "Python is not installed. Please install from the following link: https://www.python.org/ftp/python/3.9.13/python-3.9.13-amd64.exe" -ForegroundColor Red
    Pause
    exit
}

Write-Host "Installing packages from requirements.txt..." -ForegroundColor Yellow
& pip install -r requirements.txt | Out-Null
if ($LASTEXITCODE -ne 0) {
    Write-Host "Failed to install requirements.txt" -ForegroundColor Red
    Pause
    exit
}

Write-Host "Installing Chromium for Playwright..." -ForegroundColor Yellow
& playwright install chromium | Out-Null
if ($LASTEXITCODE -ne 0) {
    Write-Host "Failed to install Chromium" -ForegroundColor Red
    Pause
    exit
}

Write-Host "Checking if .env file exists..." -ForegroundColor Green
if (-Not (Test-Path .env)) {
    Write-Host ".env file does not exist, creating..." -ForegroundColor Yellow

    # Ask for related information and write to .env file
    $envData = @(
        ("LINE_CHANNEL_ID", "Please enter Line Channel ID (numeric only). Go to Line Developer -> Basic Settings to get it."),
        ("LINE_CHANNEL_ACCESS_TOKEN", "Please enter Channel access token. Go to https://developers.line.biz/console/channel/{0}/messaging-api to get it."),
        ("LINE_CHANNEL_SECRET", "Please enter Channel Secret. Go to https://developers.line.biz/console/channel/{0}/basics to get it."),
        ("NGROK_TOKEN", "Please enter ngrok authtoken. Go to https://dashboard.ngrok.com/get-started/your-authtoken to get it."),
        ("DISCORD_TOKEN", "Please enter DISCORD_TOKEN. Go to https://discord.com/developers/applications Bot menu to get it."),
        ("DISCORD_CHANNEL_ID", "Please enter DISCORD_CHANNEL_ID"),
        ("line_dev_account", "Please enter Line Account."),
        ("line_dev_password", "Please enter Line Account Password.")
    )

    $envData | ForEach-Object {
        $prompt = $_[1] -f $LINE_CHANNEL_ID
        $value = Read-Host $prompt
        Add-Content -Path .env -Value ("$($_[0])=$value")
    }
}

Write-Host "Contents of env file as follows:" -ForegroundColor Blue
Get-Content .env
OutputEncoding = [console]::InputEncoding = [console]::OutputEncoding = [Text.UTF8Encoding]::UTF8

Write-Host "Please confirm the content is correct, press any key to continue..." -ForegroundColor Green
Pause

# Read NGROK_TOKEN from .env file
$env:NGROK_TOKEN = (Get-Content .env | Select-String "NGROK_TOKEN=").ToString().Split("=")[1]

Start-Process powershell -ArgumentList "-command `"./ngrok authtoken $($env:NGROK_TOKEN); ./ngrok http https://localhost:8000`""
if ($LASTEXITCODE -ne 0) {
    Write-Host "Failed to start ngrok" -ForegroundColor Red
    Pause
    exit
}

# 等待 ngrok 完全啟動
Start-Sleep -Seconds 1

# 查詢 ngrok API 獲取隧道信息
$tunnels = Invoke-RestMethod 'http://localhost:4040/api/tunnels'
$externalUrl = $tunnels.tunnels[0].public_url

Write-Host "External URL: $externalUrl"

$env:LINE_CHANNEL_ID = (Get-Content .env | Select-String "LINE_CHANNEL_ID=").ToString().Split("=")[1]
$lineChannelUrl = "https://developers.line.biz/console/channel/$($env:LINE_CHANNEL_ID)/messaging-api"
$pythonPath = (Get-Command python).Source

# 調用 Python 腳本並傳遞外部 URL 和 LINE 頻道 URL
& $pythonPath scripts/webhook_api.py $externalUrl $lineChannelUrl
if ($LASTEXITCODE -ne 0) {
    Write-Host "Failed to update webhook URL" -ForegroundColor Red
    Write-Host "Please go to $lineChannelUrl to set Webhook URL" -ForegroundColor Yellow
    Write-Host "After adding, press any key to continue..." -ForegroundColor Green
    $null = Read-Host # 等待用戶按下 Enter 鍵
    $LASTEXITCODE = 0 # 重置錯誤碼
}

Write-Host "Starting bot... by $pythonPath" -ForegroundColor Green

# Start Line bot
Start-Process cmd -ArgumentList "/c `"$pythonPath bot_line.py`""
if ($LASTEXITCODE -ne 0) {
    Write-Host "Failed to start bot_line.py" -ForegroundColor Red
    Pause
    exit
}

# Start Discord bot
Start-Process cmd -ArgumentList "/c `"$pythonPath bot_discord.py`""
if ($LASTEXITCODE -ne 0) {
    Write-Host "Failed to start bot_discord.py" -ForegroundColor Red
    Pause
    exit
}

Write-Host "Operation complete, please keep the other three windows open."
Pause
