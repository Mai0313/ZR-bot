@echo off

echo Creating Executable...
pyinstaller --onefile --icon=zr.ico --clean --add-data="captcha_resolver/;captcha_resolver/" --distpath=./dist bot_discord.py
pyinstaller --onefile --icon=zr.ico --clean --add-data="captcha_resolver/;captcha_resolver/" --distpath=./dist bot_line.py

echo Compressing files...
powershell -command "Compress-Archive -Path ./playwright_src, ngrok.exe, get_start.md, ./dist/bot_discord.exe, ./dist/bot_line.exe, .env.example, ./bot_discord.spec, ./setting.yaml, ./permission.yaml, ./permission.json -DestinationPath release.zip"

echo Removing build and dist directories...
rmdir /s /q build
rmdir /s /q dist

echo Removing spec file...
del bot_discord.spec
del bot_line.spec

echo Done! release.zip is ready for upload.
pause
