from omegaconf import OmegaConf
from playwright.sync_api import sync_playwright

from environment import PAYMENT_EMAIL, PAYMENT_LINK

cfg = OmegaConf.load("configs/setting.yaml")

class PaymentDetailsFetcherSync:
    """這個function主要目的是模擬使用者進行開單."""
    def __init__(self):
        self.playwright = None
        self.browser = None

    def start_browser(self):
        self.playwright = sync_playwright().start()
        self.browser = self.playwright.chromium.launch(headless=True)

    def get_payment_details(self, price):
        context = self.browser.new_context(
            user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3',
            java_script_enabled=True,
            accept_downloads=False,
            has_touch=False,
            is_mobile=False,
            locale='en-US',
            permissions=[],
            geolocation=None,
            color_scheme='light',
            timezone_id='Asia/Shanghai'
        )
        page = context.new_page()
        page.goto(PAYMENT_LINK, wait_until="domcontentloaded")

        page.fill("#UnitPrice_Web", str(price))
        page.fill("#PayerMail", PAYMENT_EMAIL)
        page.click("[name='confirm_order']")
        page.click("#submitBtn")
        page.wait_for_selector(".pay_data tr:nth-child(1) > .order_td_content")

        payment_code = page.text_content(".pay_data tr:nth-child(1) > .order_td_content")
        payment_code = payment_code.replace("〈請記錄此代碼，至7-11、全家、OK超商、萊爾富超商多媒體機台列印繳費單。〉",  "")  # noqa: E501, RUF001, RUF100
        payment_amount = page.text_content("tr:nth-child(4) > .order_td_content")
        payment_date = page.text_content("tr:nth-child(5) > .order_td_content")
        order_id_temp = page.text_content("tr:nth-child(2) > .order_td_content:nth-child(2)")
        order_id = page.text_content("tr:nth-child(3) > .order_td_content")
        element = page.query_selector("img:nth-child(2)")
        bounding_box = element.bounding_box()
        screenshot = page.screenshot(clip=bounding_box)
        return payment_code, payment_amount, payment_date, order_id, screenshot

    def close_browser(self):
        self.browser.close()
        self.playwright.stop()
