from __future__ import annotations

import requests
from linebot.v3.messaging import FlexContainer, FlexMessage, MessagingApi, ReplyMessageRequest, TextMessage
from linebot.v3.messaging.models.push_message_request import PushMessageRequest

from environment import LINE_NOTIFY_TOKEN


class LineResponse:
    def __init__(self):
        pass

    def reply_message(self, api_client, event, messages):
        line_bot_api = MessagingApi(api_client)
        line_bot_api.reply_message_with_http_info(
            ReplyMessageRequest(
                reply_token=event.reply_token,
                messages=[TextMessage(text=messages)]
            )
        )

    def reply_option_message(self, api_client, event, template_message):
        line_bot_api = MessagingApi(api_client)
        line_bot_api.reply_message(
            ReplyMessageRequest(
                reply_token=event.reply_token,
                messages=[template_message],
            )
        )

    def push_flex_message(self, api_client, target_id, contents, custom_alt_text = "Flex Message"):
        flex_contents = FlexContainer.from_dict(contents)
        flex_message = FlexMessage(
            alt_text=custom_alt_text,
            contents=flex_contents  # 使用 FlexContainer 對象
        )
        line_bot_api = MessagingApi(api_client)
        line_bot_api.push_message(
            PushMessageRequest(
                to=target_id,
                messages=[flex_message],
            )
        )

    def reply_flex_message(self, api_client, event, contents, custom_alt_text = "Flex Message"):
        flex_contents = FlexContainer.from_dict(contents)
        flex_message = FlexMessage(
            alt_text=custom_alt_text,
            contents=flex_contents  # 使用 FlexContainer 對象
        )
        line_bot_api = MessagingApi(api_client)
        line_bot_api.reply_message(
            ReplyMessageRequest(
                reply_token=event.reply_token,
                messages=[flex_message],
            )
        )

    def get_image(self):
        with open("captcha/2TP38.png", "rb") as f:
            return f.read()

    def send_notify_message(self, message: str, image: bytes | None = None):
        """尬廣用的line notify function."""
        token = LINE_NOTIFY_TOKEN
        headers = { "Authorization": "Bearer " + token }
        data = { 'message': message }
        if image and message:
            requests.post("https://notify-api.line.me/api/notify", headers = headers, data = data, files = {"imageFile": image})
        else:
            requests.post("https://notify-api.line.me/api/notify", headers = headers, data = data)

if __name__ == "__main__":
    message = """排版測試"""
    image = LineResponse().get_image()
    LineResponse().send_notify_message(message)
