import pandas as pd


class LayoutGenerator:
    def __init__(self):
        pass

    def create_carousel(data_groups: pd.DataFrame, title_template: str, fields: list):
        """自動生成 carousel 的內容.

        Args:
            data_groups: 利用groupby以後的dataframe
            title_template: 用來生成title的template
            fields: 用來生成內容的欄位, 這裡為list[dict]
            例如:
                fields = [
                    {"title": "使用者名稱", "key": "使用者名稱"},
                    {"title": "權限", "key": "權限"}
                ].

        Returns:
            contents: 生成的carousel內容.
        """
        contents = {
            "type": "carousel",
            "contents": []
        }
        for group_title, group in data_groups:
            bubble_content = {
                "type": "bubble",
                "size": "mega",
                "header": {
                    "type": "box",
                    "layout": "vertical",
                    "backgroundColor": "#f0f4f7",
                    "contents": [
                        {
                            "type": "text",
                            "text": title_template.format(group_title[0]),
                            "size": "lg",
                            "weight": "bold",
                            "align": "center",
                        },
                    ],
                },
                "body": {
                    "type": "box",
                    "layout": "vertical",
                    "backgroundColor": "#f0f4f7",
                    "contents": [
                        {
                            "type": "box",
                            "layout": "horizontal",
                            "contents": [{"type": "text", "text": field['title'], "weight": "bold", "align": "center", "size": "sm"} for field in fields]
                        },
                        {"type": "separator", "color": "#696969", "margin": "sm"},
                    ],
                    "paddingStart": "0px",
                    "paddingEnd": "0px"
                }
            }
            for index, row in group.iterrows():
                data_box = {
                    "type": "box",
                    "layout": "horizontal",
                    "contents": [{"type": "text", "text": str(row[field['key']]), "wrap": True, "size": "xxs", "align": "center", "weight": "bold"} for field in fields],
                    "justifyContent": "space-between",
                    "spacing": "md",
                    "margin": "none",
                    "height": "50px",
                }
                bubble_content['body']['contents'].append(data_box)
            contents['contents'].append(bubble_content)
        return contents

    def create_flex_bubble(self, cfg, payment_title, payment_title_color, payment_code, payment_date, created_time, order_id, payment_status, payment_amount):
        flex_contents = {
            "type": "bubble",
            "size": "mega",
            "direction": "ltr",
            "hero": {
                "type": "image",
                "url": f"{cfg.image.image_url}",
                "size": "full",
                "aspectRatio": "20:13",
                "aspectMode": "cover",
            },
            "body": {
                "type": "box",
                "layout": "vertical",
                "contents": [
                    {"type": "text", "text": f"{payment_title}", "weight": "bold", "size": "xl", "color": f"{payment_title_color}"},
                    {
                        "type": "box",
                        "layout": "vertical",
                        "margin": "lg",
                        "spacing": "sm",
                        "contents": [
                            {
                                "type": "box",
                                "layout": "baseline",
                                "spacing": "sm",
                                "contents": [
                                    {
                                        "type": "text",
                                        "text": "繳費代碼",
                                        "color": "#aaaaaa",
                                        "size": "sm",
                                        "flex": 0,
                                        "weight": "bold",
                                    },
                                    {
                                        "type": "text",
                                        "text": f"{payment_code}",
                                        "wrap": True,
                                        "color": "#666666",
                                        "size": "sm",
                                        "flex": 5,
                                        "weight": "bold",
                                    },
                                ],
                            },
                            {
                                "type": "box",
                                "layout": "baseline",
                                "spacing": "sm",
                                "contents": [
                                    {
                                        "type": "text",
                                        "text": "繳費時間",
                                        "color": "#aaaaaa",
                                        "size": "sm",
                                        "flex": 0,
                                        "weight": "bold",
                                    },
                                    {
                                        "type": "text",
                                        "text": f"{payment_date}",
                                        "wrap": True,
                                        "color": "#666666",
                                        "size": "sm",
                                        "flex": 5,
                                        "weight": "bold",
                                    },
                                ],
                            },
                            {
                                "type": "box",
                                "layout": "baseline",
                                "spacing": "sm",
                                "contents": [
                                    {
                                        "type": "text",
                                        "text": "開單時間",
                                        "color": "#aaaaaa",
                                        "size": "sm",
                                        "flex": 0,
                                        "weight": "bold",
                                    },
                                    {
                                        "type": "text",
                                        "text": f"{created_time}",
                                        "wrap": True,
                                        "color": "#666666",
                                        "size": "sm",
                                        "flex": 5,
                                        "weight": "bold",
                                    },
                                ],
                            },
                            {
                                "type": "box",
                                "layout": "baseline",
                                "spacing": "sm",
                                "contents": [
                                    {
                                        "type": "text",
                                        "text": "訂單編號",
                                        "color": "#aaaaaa",
                                        "size": "sm",
                                        "flex": 0,
                                        "weight": "bold",
                                    },
                                    {
                                        "type": "text",
                                        "text": f"{order_id}",
                                        "wrap": True,
                                        "color": "#666666",
                                        "size": "sm",
                                        "flex": 5,
                                        "weight": "bold",
                                    },
                                ],
                            },
                            {
                                "type": "box",
                                "layout": "baseline",
                                "spacing": "sm",
                                "contents": [
                                    {
                                        "type": "text",
                                        "text": "訂單狀態",
                                        "color": "#aaaaaa",
                                        "size": "sm",
                                        "flex": 0,
                                        "weight": "bold",
                                    },
                                    {
                                        "type": "text",
                                        "text": f"{payment_status}",
                                        "wrap": True,
                                        "color": "#666666",
                                        "size": "sm",
                                        "flex": 5,
                                        "weight": "bold",
                                    },
                                ],
                            },
                            {
                                "type": "box",
                                "layout": "baseline",
                                "contents": [
                                    {
                                        "type": "text",
                                        "text": "付款金額",
                                        "flex": 0,
                                        "size": "sm",
                                        "color": "#aaaaaa",
                                        "weight": "bold",
                                    },
                                    {
                                        "type": "text",
                                        "text": f"{payment_amount}",
                                        "flex": 5,
                                        "size": "sm",
                                        "color": "#666666",
                                        "weight": "bold",
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
            "footer": {
                "type": "box",
                "layout": "vertical",
                "spacing": "sm",
                "contents": [
                    {"type": "separator"},
                    {
                        "type": "button",
                        "style": "link",
                        "height": "sm",
                        "action": {
                            "type": "postback",
                            "label": f"{cfg.finance.last_payment}",
                            "data": f"{cfg.finance.last_payment}",
                        },
                    },
                    {
                        "type": "button",
                        "style": "link",
                        "height": "sm",
                        "action": {
                            "type": "postback",
                            "label": f"{cfg.finance.last_five_payment}",
                            "data": f"{cfg.finance.last_five_payment}",
                        },
                    },
                    {
                        "type": "button",
                        "action": {
                            "type": "postback",
                            "label": f"{cfg.finance.all_payment_of_the_month}",
                            "data": f"{cfg.finance.all_payment_of_the_month}",
                        },
                    },
                ],
                "flex": 0,
            },
        }
        return flex_contents
