import os

from environment import OUTPUT_DIR, PAYMENT_EMAIL
from src.datamodule.database import Database
from src.utils.get_price import get_price_to_int


class DatabaseRecorder:

    def __init__(self):
        os.makedirs(OUTPUT_DIR, exist_ok=True)

    def get_history(self, current_time, payment_code, payment_amount, payment_date, order_id):
        data = [{
            "訂單建立日期": current_time,
            "付款代碼": payment_code,
            "付款金額": payment_amount,
            "付款日期": payment_date,
            "藍新訂單編號": order_id,
            "電子信箱": PAYMENT_EMAIL,
            "交易狀態": "未付款",
            "payment_notify_discord": "未通知",
            "payment_notify_line": "未通知",
        }]
        Database().save_data_to_db(data = data, table_name = "payment", strategy = "append")
        # data = Database().get_data_from_db("payment")
        # data = pd.DataFrame(data)
        # if os.path.exists(PAYMENT_FILE):
        #     data.to_csv(PAYMENT_FILE, index=None, mode="a", header=False)
        # else:
        #     data.to_csv(PAYMENT_FILE, index=None, mode="w")

    def get_accounting_from_payment_code(self, payment_code):
        data = Database().get_data_from_db("payment")
        data = data.query("付款代碼 == @payment_code")
        amount, created_time, status = None, None, False
        if not data.empty:
            amount = data['付款金額'].values[0]
            created_time = data['訂單建立日期'].values[0]
            self.get_accounting(created_time, amount)
            status = True
        return payment_code, amount, created_time, status

    def get_accounting(self, created_time, amount):
        status, amount = get_price_to_int(amount)
        data = [{
            "入帳日": created_time,
            "金額": amount,
        }]
        Database().save_data_to_db(data = data, table_name = "accounting", strategy = "append")
