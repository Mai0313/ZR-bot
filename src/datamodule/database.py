import pandas as pd
import requests
from sqlalchemy import create_engine


class Database:
    def __init__(self):
        pass

    def send_data_to_server(self, data, table_name):
        url = "http://mai0313.com:7777/save_payment_history"
        payload = {
            "data": data,
            "table_name": table_name
        }
        requests.post(url, json=payload)

    def get_data_from_db(self, table_name):
        engine = create_engine('postgresql://wei:a1z2s3x4@mai0313.com:5432/payment_history')
        query = f"SELECT * FROM {table_name}"
        data = pd.read_sql(query, engine)
        return data

    def save_data_to_db(self, data: pd.DataFrame, table_name: str, strategy: str):
        """將數據存入database.

        Args:
            data (pd.DataFrame): 數據
            table_name (str): 資料表名稱
            strategy (str):
                'fail': idk
                'replace': 替代
                'append': 新增 (合併/往下插入)


        Returns:
            None
        """
        if isinstance(data, list):
            data = pd.DataFrame(data)
        engine = create_engine('postgresql://wei:a1z2s3x4@mai0313.com:5432/payment_history')
        data.to_sql(table_name, engine, if_exists=strategy, index=False)
