def get_price_to_int(price):
    if isinstance(price, int):
        return True, price
    else:
        price = price.replace(" ", "")
        price = price.replace("//", "")
        price = price.replace("記帳", "")
        price = price.replace("查帳", "")
        price = price.replace("開單", "")
        price = price.replace("入帳", "")
        price = price.replace("NT", "")
        price = price.replace("nt", "")
        price = price.replace("元", "")
        price = price.replace("塊", "")
        price = price.replace("$", "")
        # check if price is a number
        if price.isdigit():
            return True, int(price)
        else:
            return None, price

def get_pure_order_id(order_id):
    order_id = order_id.replace(" ", "")
    order_id = order_id.replace("//", "")
    order_id = order_id.replace("記帳", "")
    order_id = order_id.replace("查帳", "")
    order_id = order_id.replace("開單", "")
    order_id = order_id.replace("查單", "")
    order_id = order_id.replace("入帳", "")
    order_id = order_id.replace("NT", "")
    order_id = order_id.replace("nt", "")
    order_id = order_id.replace("元", "")
    order_id = order_id.replace("塊", "")
    order_id = order_id.replace("$", "")
    return True, order_id
