import datetime

import pytz
from discord import Embed


class CreateMessage:
    def __init__(self):
        pass

    def edit_embed_message(self, title: str, description: str, status: bool):
        taiwan = pytz.timezone('Asia/Taipei')
        embed_msg = Embed(
            title=title,
            description=description,
            color=0xFF0000,  # Set the color of the side stripe of the embed
            timestamp=datetime.datetime.now(taiwan)
        )
        # embed_msg.set_footer(text=f"Message ID: {message.id} | Message Detected: {message.content}")
        # embed_msg.set_author(name=message.author.name, icon_url=message.author.avatar)
        # embed_msg.set_thumbnail(url=message.author.avatar)
        if status:
            embed_msg.color = 0x00FF00
        elif not status:
            embed_msg.color = 0xFF0000
        return embed_msg
