import base64

from omegaconf import OmegaConf
from playwright.async_api import async_playwright
from playwright.sync_api import sync_playwright

from captcha_resolver import CaptchaResolver
from environment import PAYMENT_UPDATE_LINK

cfg = OmegaConf.load("configs/setting.yaml")

class NewebpayCrawler:
    def __init__(self):
        self.playwright = None
        self.browser = None

    async def start_browser(self):
        self.playwright = await async_playwright().__aenter__()
        # self.browser = await self.playwright.chromium.launch()
        self.browser = await self.playwright.chromium.launch(headless=True)

    async def get_transaction_details(self, trad_id, email):
        trad_id = str(trad_id)
        context = await self.browser.new_context(
            user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3',
            java_script_enabled=True,
            accept_downloads=False,
            has_touch=False,
            is_mobile=False,
            locale='en-US',
            permissions=[],
            geolocation=None,
            color_scheme='light',
            timezone_id='Asia/Shanghai'
        )
        page = await context.new_page()

        await page.goto(PAYMENT_UPDATE_LINK, wait_until="domcontentloaded")

        await page.fill("#OrderMail", email)
        await page.fill("#TranID", trad_id)

        captcha_element = await page.query_selector("#cp_img")
        # get captcha by screenshot
        captcha_buffer = await captcha_element.screenshot(type="png")
        captcha_base64 = base64.b64encode(captcha_buffer).decode('utf-8')
        captcha_code = CaptchaResolver().get_captcha_code(captcha_base64)

        await page.fill("#CheckCode", captcha_code)
        await page.click("#div_send_btn")

        try:
            # Extracting required data after form submission
            await page.wait_for_selector("#search_result > div:nth-child(1) > div:nth-child(2)")
            order_id = await page.text_content("#search_result > div:nth-child(1) > div:nth-child(2)")
            order_id = int(order_id)
            payment_status = await page.text_content("div:nth-child(2) > div:nth-child(2)")
            payment_date = await page.text_content("div:nth-child(3) > div:nth-child(2)")
            payment_amount = await page.text_content("#search_result > div:nth-child(4) > div:nth-child(2)")
            payment_description = await page.text_content("#search_result > div:nth-child(5) > div:nth-child(2)")
        except:  # noqa: E722
            order_id, payment_status, payment_date, payment_amount, payment_description = (None,) * 5
        return order_id, payment_status, payment_date, payment_amount, payment_description

    async def close_browser(self):
        await self.browser.close()
        await self.playwright.stop()

class NewebpayCrawlerSync:
    def __init__(self):
        self.playwright = None
        self.browser = None

    def start_browser(self):
        self.playwright = sync_playwright().start()
        self.browser = self.playwright.chromium.launch(headless=True)

    def get_transaction_details(self, trad_id, email):
        trad_id = str(trad_id)
        context = self.browser.new_context(
            user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3',
            java_script_enabled=True,
            accept_downloads=False,
            has_touch=False,
            is_mobile=False,
            locale='en-US',
            permissions=[],
            geolocation=None,
            color_scheme='light',
            timezone_id='Asia/Shanghai'
        )
        page = context.new_page()

        page.goto(PAYMENT_UPDATE_LINK, wait_until="domcontentloaded")

        page.fill("#OrderMail", email)
        page.fill("#TranID", trad_id)

        captcha_element = page.query_selector("#cp_img")
        # get captcha by screenshot
        captcha_buffer = captcha_element.screenshot(type="png")
        captcha_base64 = base64.b64encode(captcha_buffer).decode('utf-8')
        captcha_code = CaptchaResolver().get_captcha_code(captcha_base64)

        page.fill("#CheckCode", captcha_code)
        page.click("#div_send_btn")

        try:
            # Extracting required data after form submission
            page.wait_for_selector("#search_result > div:nth-child(1) > div:nth-child(2)")
            order_id = int(page.text_content("#search_result > div:nth-child(1) > div:nth-child(2)"))
            payment_status = page.text_content("div:nth-child(2) > div:nth-child(2)")
            payment_date = page.text_content("div:nth-child(3) > div:nth-child(2)")
            payment_amount = page.text_content("#search_result > div:nth-child(4) > div:nth-child(2)")
            payment_description = page.text_content("#search_result > div:nth-child(5) > div:nth-child(2)")
        except:  # noqa: E722
            order_id, payment_status, payment_date, payment_amount, payment_description = (None,) * 5
        return order_id, payment_status, payment_date, payment_amount, payment_description

    def close_browser(self):
        self.browser.close()
        self.playwright.stop()
