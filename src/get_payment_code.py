from omegaconf import OmegaConf
from playwright.async_api import async_playwright

from environment import PAYMENT_EMAIL, PAYMENT_LINK

cfg = OmegaConf.load("configs/setting.yaml")

class PaymentDetailsFetcher:
    """這個function主要目的是模擬使用者進行開單."""
    def __init__(self):
        self.playwright = None
        self.browser = None

    async def start_browser(self):
        self.playwright = await async_playwright().__aenter__()
        self.browser = await self.playwright.chromium.launch(headless=True)

    async def get_payment_details(self, price):
        context = await self.browser.new_context(
            user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3',
            java_script_enabled=True,
            accept_downloads=False,
            has_touch=False,
            is_mobile=False,
            locale='en-US',
            permissions=[],
            geolocation=None,
            color_scheme='light',
            timezone_id='Asia/Shanghai'
        )
        page = await context.new_page()

        await page.goto(PAYMENT_LINK, wait_until="domcontentloaded")

        await page.fill("#UnitPrice_Web", str(price))
        await page.fill("#PayerMail", PAYMENT_EMAIL)
        await page.click("[name='confirm_order']")
        await page.click("#submitBtn")
        await page.wait_for_selector(".pay_data tr:nth-child(1) > .order_td_content")

        payment_code = await page.text_content(".pay_data tr:nth-child(1) > .order_td_content")
        payment_code = payment_code.replace("〈請記錄此代碼，至7-11、全家、OK超商、萊爾富超商多媒體機台列印繳費單。〉",  "")  # noqa: RUF001
        payment_amount = await page.text_content("tr:nth-child(4) > .order_td_content")
        payment_date = await page.text_content("tr:nth-child(5) > .order_td_content")
        order_id_temp = await page.text_content("tr:nth-child(2) > .order_td_content:nth-child(2)")
        order_id = await page.text_content("tr:nth-child(3) > .order_td_content")
        element = await page.query_selector("img:nth-child(2)")
        bounding_box = await element.bounding_box()
        screenshot = await page.screenshot(clip=bounding_box)
        return payment_code, payment_amount, payment_date, order_id, screenshot

    async def close_browser(self):
        await self.browser.close()
        await self.playwright.stop()
