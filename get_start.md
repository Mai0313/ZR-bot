# For Users

### Set up Environment

1. `sudo apt-get update`
2. `setup.sh`

# For Developers

### Docker

```bash
docker build -t discord_bot .
docker run -d discord_bot
```

### Set up Environment

```bash
pip install -r requirements.txt
playwright install
```

### Setting up https Verification

#### You may skip it if you are using a public server

```bash
./ngrok authtoken 2TON3jyVJ2j4jNZvISnMYvtOfHx_6Exe8ceyFNL3zer7ZYuV9
./ngrok http https://localhost:8000
```

#### windows

```powershell
$env:PLAYWRIGHT_BROWSERS_PATH="playwright_src"
playwright install chromium
```

#### Linux

```bash
export PLAYWRIGHT_BROWSERS_PATH="playwright_src"
playwright install chromium
```

### Clean git folder

```bash
java -jar bfg.jar --strip-blobs-bigger-than 10M
```

### Then run the Discord bot by

```bash
python bot_discord.py
```

### Then run the Web/Linebot by

```bash
python bot_line.py
```

### 封裝

```bash
pip install discord.py omegaconf playwright pandas flask line-bot-sdk cryptography python-dotenv onnxruntime numpy pillow opencv-python pre-commit beautifulsoup4
pyinstaller --onefile --icon=zr.ico --clean --add-data="captcha_resolver/;captcha_resolver/" --distpath=./dist bot_discord.py

sudo apt-get install libgl1-mesa-glx
```
